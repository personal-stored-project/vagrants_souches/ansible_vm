echo "---Install Dependencies---"
apt-get update
apt-get upgrade -y

# TMP for Work on ansible, please remove these ligne after work.
echo "---Change sshd config for pass auth---"
FINDTHIS="PasswordAuthentication no"
TOTHIS="PasswordAuthentication yes"
sed -i -e "s/$FINDTHIS/$TOTHIS/g" /etc/ssh/sshd_config
systemctl restart sshd

# END TMP
if [ "$1" == "true" ]
then
    echo "---Install Ansible---"
    apt-get install sshpass ansible git -y

    echo "---Créations et mise en place des clefs SSH---"
    su vagrant -c "ssh-keygen -q -t rsa -N '' -f /home/vagrant/.ssh/id_rsa"
    IFS='/' read -r -a node <<< "$2"
    for i in "${node[@]}"; do
        sshpass -p vagrant ssh-copy-id -i /home/vagrant/.ssh/id_rsa.pub -o StrictHostKeyChecking=no vagrant@$i
    done
    echo $node > /home/vagrant/host
fi